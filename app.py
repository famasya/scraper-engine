from flask import Flask, request, jsonify
from engine import scraper
app = Flask(__name__)

@app.route('/')
def home():
	args = request.args
	site = args.get('site')
	start = args.get('start')
	end = args.get('end')
	if site is None or start is None or end is None:
		return ''

	result = scraper(site, start, end)
	if len(result) == 0:
		return 'We might be blocked'

	return jsonify(result)

if __name__ == '__main__':
	app.run(port=5050, debug=True)
