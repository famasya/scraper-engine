import pprint
import serpscrap

keywords = ['site:cnnindonesia.com -tag -video']

config = serpscrap.Config()
config.set('sel_browser', 'chrome')
config.set('num_pages_for_keyword', 5)
config.set('start_date', '11/1/2017')
config.set('end_date', '11/2/2017')
config.set('sleeping_min', 5)
config.set('do_caching', False)
config.set('screenshot', False)
config.set('executable_path', '/Users/famasya/Dev/Python/Work/scraper-engine/chromedriver')

scrap = serpscrap.SerpScrap()
scrap.init(config=config.get(), keywords=keywords)
results = scrap.run()

for result in results:
    pprint.pprint(result)
    print()
