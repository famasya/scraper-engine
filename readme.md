## How to Run

Create virtual environment first using python3.  
`virtualenv venv --python=python3.6`

Install requirements:  
`pip install -r requirements.txt`

Download chrome driver for your OS. Put it into this dir. And set `executable_path` in the engine.py pointing to your chrome driver

Run with command  
`python engine.py "site" "start date" "end date"`  
date format: MM/DD/YYYY

Example:  
`python engine.py kompas 11/1/2017 11/2/2017`