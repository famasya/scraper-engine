import pprint
import serpscrap
import argparse

def scraper(site, start_date, end_date):
    keywords = {
        'kompas': 'site:kompas.com inurl:"read"',
        'tempo': 'site:tempo.co inurl:"read"',
        'antaranews': 'site:antaranews.com inurl:"berita"',
        'huffingtonpost': 'site:huffingtonpost.com inurl:"entry"',
        'cnn': 'site:edition.cnn.com inurl:"index.html"',
        'nytimes': 'site:nytimes.com inurl:html',        
        'foxnews': 'site:foxnews.com inurl:html',        
        'nbc': 'site:nbcnews.com', # need url filter        
        'wsj': 'site:wsj.com inurl:articles',        
        'guardian': 'site:theguardian.com', # need url filter    
        'chinadaily': 'site:chinadaily.com.cn inurl:htm',        
        'rt': 'site:www.rt.com', # need url filter        
        'dw': 'site:www.dw.com/en/', # need url filter
        'ipotnews': 'site:www.ipotnews.com',        
    }
    
    keyword = keywords[site]

    config = serpscrap.Config()
    config.set('sel_browser', 'chrome')
    config.set('num_pages_for_keyword', 100)
    config.set('start_date', start_date)
    config.set('end_date', end_date)
    config.set('screenshot', False)
    config.set('sleeping_min', 5)

    # Set executable path for Chrome Driver
    config.set('executable_path', '/Users/famasya/Dev/Python/Work/scraper-engine/chromedriver')

    scrap = serpscrap.SerpScrap()
    scrap.init(config=config.get(), keywords=keyword)
    results = scrap.run()

    res = []
    for result in results:
        res.append(result)

    return res
